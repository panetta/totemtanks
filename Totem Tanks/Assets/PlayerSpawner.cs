﻿using UnityEngine;
using System.Collections;
using UnityEngine.Networking;

public class PlayerSpawner : NetworkBehaviour {

    public override void OnStartServer()
    {
        
    }

    public override void OnStartClient()
    {
        // register client events, enable effects
        Debug.Log("try to create player");

        if (ClientScene.localPlayers.Count == 0)
        {
            Debug.Log("no player");
            ClientScene.AddPlayer(NetworkManager.singleton.client.connection, 0);
        }

        //NetworkManager netman = GameObject.FindObjectOfType<NetworkManager>();
        //NetworkManager.singleton.client.connection
        //ClientScene.AddPlayer(NetworkManager.singleton.client.connection, 0);
    }

    public virtual void OnClientSceneChanged(NetworkConnection conn)
    {
        Debug.Log("scene changed");

    }


    public override void OnStartLocalPlayer()
    {
        // register client events, enable effects
        Debug.Log("local  player start");
    }
    

    // Use this for initialization
    void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}
}
