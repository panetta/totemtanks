﻿using UnityEngine;
using System.Collections;
using UnityEngine.Networking;

public class ClientSpawnTest : NetworkBehaviour {

    public GameObject AmmoPrefab;
    public float FiringSpeed;

    // Use this for initialization
    void Start () {
	
	}

    // Update is called once per frame
    void Update()
    {
        if (this.GetComponent<NetworkIdentity>().isLocalPlayer)
        {
            if (Input.GetMouseButtonDown(1))
            {
                CmdFire();
            }
        }
    }

    [Command]
    public void CmdFire()
    {
        GameObject ammo = (GameObject)Instantiate(AmmoPrefab, transform.position + this.transform.forward, transform.rotation);
        ammo.GetComponent<Rigidbody>().AddForceAtPosition(this.transform.forward * FiringSpeed, new Vector3());
        NetworkServer.Spawn(ammo);
        
    }
}
