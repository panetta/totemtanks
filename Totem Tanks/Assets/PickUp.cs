﻿using UnityEngine;
using System.Collections;
using UnityEngine.Networking;

public class PickUp : MonoBehaviour {

    public bool PickUpAllowed;
    private Canvas _canvas;

    // Use this for initialization
	void Start ()
	{
	    _canvas = GetComponentInChildren<Canvas>();
	}
	
	// Update is called once per frame
	void Update ()
	{
        //if (PlayerCamera.CurrentCam != null)
        //{
        //    var newCanvasPos = (PlayerCamera.CurrentCam.transform.position - transform.position).normalized * 0.3f;
        //    _canvas.transform.position = new Vector3(newCanvasPos.x, _canvas.transform.position.y, newCanvasPos.z);
        //}
    }

    void OnCollisionEnter(Collision collision)
    {
        
        //PlayerTower tower = collision.collider.gameObject.transform.root.GetComponent<PlayerTower>();
        //if (tower != null)
        //{
        //    this.transform.position = collision.collider.gameObject.transform.root.gameObject.transform.position + (Vector3.up * 1f);
        //    this.transform.SetParent(collision.collider.gameObject.transform.root.gameObject.transform);
        //    // Debug.Log(collision.collider.gameObject.transform.root.name);
        //}


        PlayerTower tower = collision.collider.gameObject.transform.root.GetComponent<PlayerTower>();
        if (tower != null && PickUpAllowed)
        {
            tower.AddComponent(this.transform.gameObject);
            PickUpAllowed = false;
            StaticManager.Instance.PlayTotemPickUp();

            //this.transform.position = collision.collider.gameObject.transform.root.gameObject.transform.position + (Vector3.up * 1f);
            //this.transform.SetParent(collision.collider.gameObject.transform.root.gameObject.transform);
            // Debug.Log(collision.collider.gameObject.transform.root.name);
        }



        //foreach (ContactPoint contact in collision.contacts)
        //{
        //    //Debug.DrawRay(contact.point, contact.normal, Color.white);
        //    Debug.Log("Collision");

        //}
    }
}
