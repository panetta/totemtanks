﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace Assets
{
    public static class QuaternionHelper
    {
        public static Quaternion Trim(Quaternion rotation, bool trimX, bool trimY, bool trimZ)
        {
            var euler = rotation.eulerAngles;
            return Quaternion.Euler(trimX ? 0 : euler.x, trimY ? 0 : euler.y, trimZ ? 0 : euler.z);
        }
    }
}
