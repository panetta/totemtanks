﻿using Assets;
using UnityEngine;
using System.Collections;
using UnityEngine.Networking;

[RequireComponent(typeof(LineRenderer))]
public class Laser : NetworkBehaviour
{
    public float TargetAngleDamage = 5f;
    public float LaserLength = 8;
    public GameObject ImpactEffect;
    public Transform Owner;
    public Vector3 SpawnOffset;
    private IntervalTimer _explosionTimer;
    private LineRenderer _laserLine;

    // Use this for initialization
    void Start()
    {
        _laserLine = GetComponent<LineRenderer>();
        _explosionTimer = new IntervalTimer(0.2f) {AutoRestart = false};
    }

    // Update is called once per frame
    void Update()
    {
        _explosionTimer.Update();
        if (isServer || isLocalPlayer)
        {
            transform.position = Owner.position + Owner.rotation*SpawnOffset;
            transform.rotation = Owner.rotation;
        }

        var end = transform.position + transform.forward*LaserLength;

        RaycastHit hitInfo;
        if (Physics.Raycast(transform.position, transform.forward, out hitInfo, LaserLength))
        {
            end = hitInfo.point;

            if (isServer && _explosionTimer.IsElapsed)
            {
                _explosionTimer.ResetToZero();
                var receiver = hitInfo.collider.gameObject.GetComponent<TowerDamageReceiver>();
                if (receiver != null)
                {
                    receiver.ReceiveDamage(TargetAngleDamage*_explosionTimer.IntervalTimeS, transform.forward);
                }

                if (ImpactEffect != null)
                {
                    var explosion = (GameObject)Instantiate(ImpactEffect, end, transform.rotation);
                    NetworkServer.Spawn(explosion);
                }
            }
        }

        _laserLine.SetPosition(0, transform.position);
        _laserLine.SetPosition(1, end);

    }

    public void SetOwner(Transform transform1, Vector3 position)
    {
        Owner = transform1;
        SpawnOffset = transform1.InverseTransformPoint(position);
    }
}
