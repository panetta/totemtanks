﻿using UnityEngine;
using System.Collections;

public class PlayerModifiers : MonoBehaviour
{
    public float MovementSpeedMultiplier = 1f;
    public float RotationSpeedMultiplier = 1f;

    // Use this for initialization
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {

    }
}
