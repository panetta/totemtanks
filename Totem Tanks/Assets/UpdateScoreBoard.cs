﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using UnityEngine.Networking;

public class UpdateScoreBoard : NetworkBehaviour {

    public Text ScoreBoard;
    public Text Winner;
    public bool RoundFinished = false;
    public int RoundCountDown = 500;

    [SyncVar]
    public bool levelSwaped = false;

    // Use this for initialization
    void Start () {
        ScoreBoard = GameObject.Find("ScoreBoard").GetComponent<Text>();
        Winner = GameObject.Find("Winner").GetComponent<Text>();
    }
	
	// Update is called once per frame
	void Update () {

        string ScoreBoardUpdate = "";

        LocalScore[] LocalScores = FindObjectsOfType<LocalScore>();
        for (int i = 0; i < LocalScores.Length; i++)
        {
            ScoreBoardUpdate = ScoreBoardUpdate + LocalScores[i].PlayerName + " : " + LocalScores[i].PlayerScore.ToString() + "XP\r\n";

            if (LocalScores[i].PlayerScore > 1000 && !RoundFinished)
            {
                StaticManager.Instance.Win.Play();
                RoundFinished = true;
                Winner.text = "The gods have been appeased\r\n\r\n" + LocalScores[i].PlayerName + " wins\r\n\r\n" + "Next round starting....";
            }

        }
        ScoreBoard.text = ScoreBoardUpdate;

        if (RoundFinished)
        {
            RoundCountDown--;
            Debug.Log(RoundCountDown.ToString());

            if (RoundCountDown <= 0)
            {
                
                CmdLevelChange();
                
                
                //SceneManager.LoadScene("GamePlay1");
               // FindObjectOfType<NetworkManager>().pla
            }
        }
    }


    [Server]
    public void CmdLevelChange()
    {
        if (!levelSwaped)
        {
            levelSwaped = true;
            NetworkManager.singleton.ServerChangeScene("GamePlay");
        }
        
    }


}
