﻿using System.Collections.Generic;
using UnityEngine;
using System.Collections;
using UnityEngine.Networking;

public class PlayerMove : MonoBehaviour
{
    public float BaseMovementSpeed;
    public float BaseRotationSpeed;
    public float MovementSpeed;
    public float RotationSpeed;

    public float DefaultMovementMultiplier = 0.9f;
    public float DefaultRotationMultiplier = 1f;

    // Use this for initialization
    void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
        if (this.GetComponent<NetworkIdentity>().isLocalPlayer)
        {
            bool moved = false;

            if (Input.GetKey(KeyCode.W))
            {
                this.GetComponent<Rigidbody>().MovePosition(this.transform.position + this.transform.forward * MovementSpeed);
                //this.GetComponent<Rigidbody>().AddForce(this.transform.forward * MovementSpeed, ForceMode.Impulse);
                moved = true;
                StaticManager.Instance.TankRoll.volume = StaticManager.Instance.TankRoll.volume + 0.1f;
            }
            if (Input.GetKey(KeyCode.S))
            {
                this.GetComponent<Rigidbody>().MovePosition(this.transform.position + (this.transform.forward * -1) * MovementSpeed);
                moved = true;
                StaticManager.Instance.TankRoll.volume = StaticManager.Instance.TankRoll.volume + 0.1f;
            }
            if (Input.GetKey(KeyCode.A))
            {
                Quaternion deltaRotation = Quaternion.Euler(Vector3.up * -RotationSpeed * Time.deltaTime);
                this.GetComponent<Rigidbody>().MoveRotation(this.GetComponent<Rigidbody>().rotation * deltaRotation);
                moved = true;
                StaticManager.Instance.TankRoll.volume = StaticManager.Instance.TankRoll.volume + 0.1f;
            }
            if (Input.GetKey(KeyCode.D))
            {
                Quaternion deltaRotation = Quaternion.Euler(Vector3.up * RotationSpeed * Time.deltaTime);
                this.GetComponent<Rigidbody>().MoveRotation(this.GetComponent<Rigidbody>().rotation * deltaRotation);
                moved = true;
                StaticManager.Instance.TankRoll.volume = StaticManager.Instance.TankRoll.volume + 0.1f;
            }


            if (StaticManager.Instance.TankRoll.volume > 0 & !moved)
            {
                StaticManager.Instance.TankRoll.volume = StaticManager.Instance.TankRoll.volume - 0.1f;
            }

        }
    }

    public void RecalculateMovementSpeed(List<GameObject> totemComponents)
    {
        MovementSpeed = BaseMovementSpeed;
        RotationSpeed = BaseRotationSpeed;
        foreach (var totemGo in totemComponents)
        {
            var modifiers = totemGo.GetComponent<PlayerModifiers>();
            if (modifiers != null)
            {
                MovementSpeed = MovementSpeed * modifiers.MovementSpeedMultiplier;
                RotationSpeed = RotationSpeed * modifiers.RotationSpeedMultiplier;
            }
            else
            {
                MovementSpeed = MovementSpeed * DefaultMovementMultiplier;
                RotationSpeed = RotationSpeed * DefaultRotationMultiplier;
            }
        }
    }
}
