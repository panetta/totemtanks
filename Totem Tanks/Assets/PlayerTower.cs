﻿using UnityEngine;
using System.Collections;
using UnityEngine.Networking;
using System;
using System.Collections.Generic;

public class PlayerTower : NetworkBehaviour
{
    public GameObject[] AmmoType;
    public GameObject TowerSpawn;
    public List<GameObject> TotemComponents = new List<GameObject>();
    public TowerDamageControl DamageController;
    private PlayerMove _playerMove;
    //List<GameObject> TotemComponents = new List<GameObject>();

    public override void OnStartLocalPlayer()
    {
        GetComponentInChildren<MouseLook>().SetLocalPlayer(isLocalPlayer);
    }

    // Use this for initialization
    void Start() {
        DamageController = GetComponentInChildren<TowerDamageControl>();
        _playerMove = GetComponent<PlayerMove>();
    }

    // Update is called once per frame
    void Update() {
        if (this.GetComponent<NetworkIdentity>().isLocalPlayer)
        {
            if (Input.GetMouseButtonDown(0))
            {
                Gun[] theGuns = this.GetComponentsInChildren<Gun>();
                foreach (Gun aGun in theGuns)
                {
                    //CmdFire(aGun.AmmoPrefab, aGun.FiringSpeed, aGun.GetPosition(), aGun.GetRotation(), aGun.GetForward());
                    if (!aGun.CanFire())
                    {
                        continue;
                    }
                    aGun.IsFiring();

                    int aType = aGun.AmmoType;
                    float FireSpeed = aGun.FiringSpeed;
                    //Vector3 GunPosition = aGun.GetPosition();
                    Vector3 GunPosition = aGun.ProjectileSpawn.transform.position;
                    Vector3 GunForward = aGun.GetForward();
                    Quaternion GunRotation = aGun.GetRotation();

                    // PlayAudio
                    if(aType == 0)
                    {
                        StaticManager.Instance.PlayLargeCannonFire();
                    }
                    if (aType == 1)
                    {
                        StaticManager.Instance.PlayMediumCannonFire();
                    }
                    if (aType == 2)
                    {
                        StaticManager.Instance.PlayLaserCannonFire();
                    }

                    CmdFire(aType, FireSpeed, GunPosition, GunRotation, GunForward);

                    //CmdFire(aGun.AmmoPrefab, aGun.FiringSpeed, aGun.GetPosition(), aGun.GetRotation(), aGun.GetForward());


                }
            }

            //foreach (var totemGo in TotemComponents)
            //{
            //    if (totemGo != null)
            //    {
            //        totemGo.transform.LookAt(totemGo.transform.position +
            //                                 DamageController.VisibleAngle * totemGo.transform.parent.forward * 10f);
            //    }
            //}
        }
    }

    internal void AddComponent(GameObject pickUp)
    {
        Debug.Log("Trying to add component");

        TotemComponents.RemoveAll(t => t == null || t.gameObject == null);

        TotemComponents.Add(pickUp);

        if (TotemComponents.Count == 1)
        {
            // Set the parent for the tower
            pickUp.transform.SetParent(TowerSpawn.transform);
            // Move it to the right spot
            pickUp.transform.position = TowerSpawn.transform.position;
        }
        else {
            // Set the parent for the tower
            pickUp.transform.SetParent(TotemComponents[TotemComponents.Count - 2].transform);
            // Move it to the right spot
            pickUp.transform.position = TotemComponents[TotemComponents.Count - 2].GetComponentInChildren<SpawnPoint>().transform.position;
        }
        pickUp.transform.localRotation = Quaternion.identity;
        var receiver = pickUp.GetComponent<TowerDamageReceiver>();
        if (receiver != null)
        {
            receiver.Owner = this;
        }

        if (isLocalPlayer)
        {
            _playerMove.RecalculateMovementSpeed(TotemComponents);
        }
        
        //for (int i = 0; i < TotemComponents.Length; i++)
        // {
        //    if (TotemComponents[i] == null) {
        //        TotemComponents[i] = pickUp;

        //        if (i == 0)
        //        {
        //            pickUp.transform.SetParent(TowerSpawn.transform);
        //        }
        //        else {

        //            pickUp.transform.SetParent(TotemComponents[i-1].transform);
        //        }
        //    }
        // }
    }

    [Command]
    public void CmdFire(int aType, float FiringSpeed, Vector3 Position, Quaternion Rotaion, Vector3 Forward)
    {


        //GameObject ammo = (GameObject)Instantiate(AmmoType[aType], Position + Forward, Rotaion);
        GameObject ammo = (GameObject)Instantiate(AmmoType[aType], Position, Rotaion);
        //ammo.GetComponent<Rigidbody>().AddForceAtPosition(Forward * FiringSpeed, new Vector3());


        var multiShot = ammo.GetComponentsInChildren<ProxyAmmo>();
        foreach (var ammoProxy in multiShot)
        {
            GameObject ammoChild =
                (GameObject)
                Instantiate(ammoProxy.PrefabToSpawn, Position + ammoProxy.transform.localPosition,
                            Rotaion*ammoProxy.transform.localRotation);
            InitAmmo(ammoChild, Position + ammoProxy.transform.localPosition);
            NetworkServer.Spawn(ammoChild);
        }

        if (ammo.GetComponent<NetworkIdentity>() != null)
        {
            InitAmmo(ammo, Position);
            NetworkServer.Spawn(ammo);
        }

        //this.GetComponent<NetworkIdentity>().localPlayerAuthority = true;
        //NetworkServer.SpawnWithClientAuthority(ammo, PlayerRef);
    }

    private void InitAmmo(GameObject ammo, Vector3 position)
    {
        var lasers = ammo.GetComponentsInChildren<Laser>();
        foreach (var laser in lasers)
        {
            laser.SetOwner(DamageController.transform, position);

        }
        var projectiles = ammo.GetComponentsInChildren<Projectile>();
        foreach (var projectile in projectiles)
        {
            projectile.Owner = this;
        }
    }

    [ClientRpc]
    public void RpcReceiveDamage(float angleAmount, Vector3 direction)
    {
        //DamageController.AddAngle(angleAmount, direction);
    }

    public void BreakTower(GameObject breakAt)
    {
        //This should probably only be called on/from the server
        if (!isLocalPlayer && !isServer)
        {
            return;
        }

        int startIndex = 0;
        if (breakAt != null)
        {
            startIndex = TotemComponents.IndexOf(breakAt);
        }
        RpcBreakTower(startIndex);
    }

    [ClientRpc]
    public void RpcBreakTower(int startIndex)
    {

        for (int i = startIndex; i < TotemComponents.Count; i++)
        {
            TotemComponents[i].GetComponentInChildren<TowerDamageReceiver>().MakeBroken();
        }

        TotemComponents.RemoveRange(startIndex, TotemComponents.Count - startIndex);

        if (isLocalPlayer)
        {
            _playerMove.RecalculateMovementSpeed(TotemComponents);            
        }
    }
}
