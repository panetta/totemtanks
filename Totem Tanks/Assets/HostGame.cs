﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using UnityEngine.Networking;

public class HostGame : MonoBehaviour {

    public NetworkManager manager;

    void Awake()
    {
        manager = GetComponent<NetworkManager>();
    }

    // Use this for initialization
    void Start () {
        manager = GetComponent<NetworkManager>();
        GameObject.Find("HostIP").GetComponent<Text>().text = manager.networkAddress;

    }
	
	// Update is called once per frame
	void Update () {
       

    }

    public void Host()
    {
        StaticManager.Instance.PlayerName = GameObject.Find("PlayerNameText").GetComponent<Text>().text;
       Debug.Log("Auto Create player set to " + manager.autoCreatePlayer);
        manager.StartHost();
        //SceneManager.LoadScene("GamePlay", LoadSceneMode.Single);




        //manager.SetMatchHost("localhost", 1337, false);
        
        //manager.StartMatchMaker();
        


    }

    public void Client()
    {

        StaticManager.Instance.PlayerName = GameObject.Find("PlayerNameText").GetComponent<Text>().text;

       

        manager.networkAddress = GameObject.Find("HostIP").GetComponent<Text>().text;

        manager.StartClient();

        //ClientScene.Ready(manager.client.connection);

        //if (ClientScene.localPlayers.Count == 0)
        //{
        //    ClientScene.AddPlayer(manager.client.connection,0);
        //}

       

    }
}
