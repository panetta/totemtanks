﻿using UnityEngine;
using System.Collections;
using UnityEngine.Networking;

public class DestroyAfterSeconds : NetworkBehaviour
{
    public float Delay = 3f;

    // Use this for initialization
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        if (gameObject == null)
        {
            return;
        }
        Delay -= Time.deltaTime;

        if (Delay <= 0f)
        {
            NetworkServer.Destroy(gameObject);
        }
    }
}
