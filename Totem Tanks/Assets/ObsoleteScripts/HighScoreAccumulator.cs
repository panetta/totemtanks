﻿using UnityEngine;
using System.Collections;
using UnityEngine.Networking;
using UnityEngine.UI;

public class HighScoreAccumulator : NetworkBehaviour {

    [SyncVar]
    public int Player0Score = 0;
    [SyncVar]
    public int Player1Score = 0;
    [SyncVar]
    public int Player2Score = 0;
    [SyncVar]
    public int Player3Score = 0;

    [SyncVar]
    public string Player0Name;
    [SyncVar]
    public string Player1Name;
    [SyncVar]
    public string Player2Name;
    [SyncVar]
    public string Player3Name;

    //public int[] PlayerScores = new int[4];

    public int HighScoreAmount = 0;
    public string PlayerName;
    public PlayerTower playersTower;
    public int PlayerNumber = 0;

    public Text ScoreBoard;

    public override void OnStartLocalPlayer()
   {
    //    if (isLocalPlayer)
    //    {
    //        InvokeRepeating("CmdHSCalc", 1f, 1f);  
    //        PlayerName = StaticManager.Instance.PlayerName;
    //        //PlayerNumber = int.Parse(StaticManager.Instance.PlayerName);
    //    }
    }

    // Use this for initialization
    void Start () {
        ScoreBoard = GameObject.Find("ScoreBoard").GetComponent<Text>();
    }
	
	// Update is called once per frame
	void Update () {
        
	}

    //[Command]
    //void CmdHSCalc()
    //{
    //    //if (isLocalPlayer)
    //    {
    //        HighScoreAmount = HighScoreAmount + playersTower.topComponent;
    //        CmdSendHS(HighScoreAmount, PlayerName);
    //    }
    //}

    
    public void SendHS(int HighScoreAmount, string PlayerName)
    {
        //if (isLocalPlayer)
        {

            // Find out which player the name coresponds to
            if (Player0Name == PlayerName)
            {
                // Update their high score
                Player0Score = Player0Score + HighScoreAmount;
            }
            else
            {
                if (Player1Name == PlayerName)
                {
                    Player1Score = Player1Score + HighScoreAmount;
                }
                else
                {
                    if (Player2Name == PlayerName)
                    {
                        Player2Score = Player2Score + HighScoreAmount;
                    }
                    else
                    {
                        if (Player3Name == PlayerName)
                        {
                            Player3Score = Player3Score + HighScoreAmount;
                        }
                        else
                        {
                            if (Player0Name == "")
                            {
                                Player0Name = PlayerName;
                                Player0Score = Player0Score + HighScoreAmount;
                            }
                            else
                            {
                                if (Player1Name == "")
                                {
                                    Player1Name = PlayerName;
                                    Player1Score = Player1Score + HighScoreAmount;
                                }
                                else
                                {
                                    if (Player2Name == "")
                                    {
                                        Player2Name = PlayerName;
                                        Player2Score = Player2Score + HighScoreAmount;
                                    }
                                    else
                                    {
                                        // Doesn't have a slot so must be player 4  
                                        Player3Name = PlayerName;
                                        Player3Score = Player3Score + HighScoreAmount;
                                    }
                                }
                            }
                        }
                    }
                }
            }





            // Send them to the clients
            
            string HighScoresText = "";
          
            HighScoresText = HighScoresText + Player0Name + " : " + Player0Score + "XP" + "\r\n";
            HighScoresText = HighScoresText + Player1Name + " : " + Player1Score + "XP" + "\r\n";
            HighScoresText = HighScoresText + Player2Name + " : " + Player2Score + "XP" + "\r\n";
            HighScoresText = HighScoresText + Player3Name + " : " + Player3Score + "XP" + "\r\n";
         

            Scores(HighScoresText);
        }
    }

 
    void Scores(string HighScores)
    {
        Debug.Log(HighScores);
        ScoreBoard.text = HighScores;
    }

}
