﻿using Assets;
using UnityEngine;
using System.Collections;
using UnityEngine.Networking;

public class TowerDamageControl : MonoBehaviour
{
    public Quaternion CurrentAngleDestination = Quaternion.identity;
    public float ReturnToHomeSpeed = 1f;
    public float RotationSpeed = 5f;
    public Quaternion VisibleAngle = Quaternion.identity;
    public float RotationBreakAngle = 75f;

    // Use this for initialization
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        CurrentAngleDestination = Quaternion.Lerp(CurrentAngleDestination, Quaternion.identity,
                                                  Time.deltaTime * ReturnToHomeSpeed);
        Debug.DrawRay(transform.position, CurrentAngleDestination * transform.forward * 10f, Color.yellow);
        Debug.DrawRay(transform.position, VisibleAngle * transform.forward * 10f, Color.white);

        VisibleAngle = Quaternion.Lerp(VisibleAngle, CurrentAngleDestination, Time.deltaTime * RotationSpeed);
    }

    public void AddAngle(float angleAmount, Vector3 direction)
    {
        const float debugLineDuration = 0.3f;

        var rotationAxis = Vector3.Cross(Vector3.up, direction);
        var angleToAdd = Quaternion.AngleAxis(angleAmount, rotationAxis);
        var angleToAddLocal = angleToAdd*Quaternion.Inverse(transform.localRotation);

        CurrentAngleDestination = angleToAdd*CurrentAngleDestination;
        //CurrentAngleDestination = CurrentAngleDestination * QuaternionHelper.Trim(angleToAddLocal, false, true, false);
        Debug.DrawRay(transform.position, transform.forward * 10f, Color.cyan, debugLineDuration);
        Debug.DrawRay(transform.position, angleToAdd * transform.forward * 10f, Color.red, debugLineDuration);
        Debug.DrawRay(transform.position, angleToAddLocal * transform.forward * 10f, Color.green, debugLineDuration);
        //Debug.DrawRay(transform.position, rotationAxis * 10f, Color.blue, debugLineDuration);

    }
}
