﻿using UnityEngine;

namespace Assets
{
    public class IntervalTimer
    {
        public float IntervalTimeS { get; set; }
        public bool AutoRestart { get; set; }
        public bool IsElapsed { get; private set; }
        private float _timeElapsed = 0f;

        public IntervalTimer(float intervalTimeS)
        {
            IntervalTimeS = intervalTimeS;
            AutoRestart = true;
        }

        public IntervalTimer Update()
        {
            return Update(Time.deltaTime);
        }

        public IntervalTimer Update(float deltaTime)
        {
            if (!AutoRestart && IsElapsed)
            {
                return this;
            }

            _timeElapsed += deltaTime;

            if (_timeElapsed >= IntervalTimeS)
            {
                _timeElapsed -= IntervalTimeS;
                IsElapsed = true;
            }
            else
            {
                IsElapsed = false;
            }

            return this;
        }

        public void ResetToZero()
        {
            _timeElapsed = 0f;
            IsElapsed = false;
        }

        public void SetToElapsed()
        {
            _timeElapsed = IntervalTimeS;
        }

        public float TimeElapsed
        {
            get { return _timeElapsed; }
        }
    }
}
