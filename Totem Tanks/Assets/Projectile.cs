﻿using UnityEngine;
using System.Collections;
using UnityEngine.Networking;

public class Projectile : NetworkBehaviour
{
    public float TargetAngleDamage = 5f;
    public GameObject ImpactEffect;
    public float ProjectileSpeed = 20f;
    public float ProjectileGravity = 0f;
    public PlayerTower Owner;

    // Use this for initialization
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        transform.position += transform.forward * ProjectileSpeed * Time.deltaTime;
        transform.position -= Vector3.up * ProjectileGravity * Time.deltaTime;
    }

    void OnTriggerEnter(Collider other)
    {
        if (!isServer)
        {
            return;
        }

        var receiver = other.GetComponent<TowerDamageReceiver>();
        if (receiver != null)
        {
            if (receiver.Owner == Owner)
            {
                return;
            }
            receiver.ReceiveDamage(TargetAngleDamage, transform.forward);
        }

        if (ImpactEffect != null)
        {
            var explosion = (GameObject)Instantiate(ImpactEffect, transform.position, transform.rotation);
            NetworkServer.Spawn(explosion);
            StaticManager.Instance.PlayExplosion();
        }
        NetworkServer.Destroy(gameObject);
    }
}
