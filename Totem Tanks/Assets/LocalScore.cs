﻿using UnityEngine;
using System.Collections;
using UnityEngine.Networking;

public class LocalScore : NetworkBehaviour {

    [SyncVar]
    public int PlayerScore;
    [SyncVar]
    public string PlayerName;

	// Use this for initialization
	void Start () {
        if (isLocalPlayer) { 
            PlayerName = StaticManager.Instance.PlayerName;
            InvokeRepeating("ScoreUpdate", 1f, 1f);
        }
    }
	
	// Update is called once per frame
	void Update ()
    {
	    
	}

    void ScoreUpdate()
    {
        if (isLocalPlayer)
        {
            int score = GetComponent<PlayerTower>().TotemComponents.Count;

            if (score < 0)
            {
                score = 0;
            }

            PlayerScore = PlayerScore + score;
            CmdUpdatePlayerScore(PlayerScore, PlayerName);
        }
    }


    [Command]
    void CmdUpdatePlayerScore(int scoreValue, string nameValue)
    {
        PlayerScore = scoreValue;
        PlayerName = nameValue;
    }

}
