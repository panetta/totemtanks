﻿using UnityEngine;
using System.Collections;
using UnityEngine.Networking;
using System;
using UnityEngine.UI;

public class Gun : MonoBehaviour
{

    public int AmmoType;
    public float FiringSpeed;
    public GameObject ProjectileSpawn;
    public float ReloadSpeed = 1f;
    public float ReloadRemaining = 0f;

    public Image ReloadContainer;
    public Image ReloadImage;
    //public GameObject PlayerRef;


    // Use this for initialization
    void Start () {
	}
	
	// Update is called once per frame
    private void Update()
    {
        //if (this.GetComponent<NetworkIdentity>().isLocalPlayer)
        //{
        //    if (Input.GetMouseButtonDown(0))
        //    {
        //        CmdFire();
        //    }
        //}
        if (ReloadRemaining > 0f)
        {
            ReloadContainer.gameObject.SetActive(true);
            ReloadRemaining -= Time.deltaTime;
            ReloadImage.fillAmount = 1f - ReloadRemaining/ReloadSpeed;
        }
        else
        {
            ReloadContainer.gameObject.SetActive(false);
        }
    }

    public Vector3 GetPosition()
    {
        return this.transform.position;
    }

    internal Vector3 GetForward()
    {
        return this.transform.forward;
    }

    public Quaternion GetRotation()
    {
        return this.transform.rotation;
    }

    public bool CanFire()
    {
        return ReloadRemaining <= 0f;
    }

    public void IsFiring()
    {
        ReloadRemaining = ReloadSpeed;
    }

    //[Command]
    //public void CmdFire()
    //{
    //    GameObject ammo = (GameObject)Instantiate(AmmoPrefab, transform.position + this.transform.forward, transform.rotation);
    //    ammo.GetComponent<Rigidbody>().AddForceAtPosition(this.transform.forward * FiringSpeed, new Vector3());
    //    //NetworkServer.Spawn(ammo);

    //    //this.GetComponent<NetworkIdentity>().localPlayerAuthority = true;


    //    NetworkServer.SpawnWithClientAuthority(ammo, PlayerRef);
    //}
}
