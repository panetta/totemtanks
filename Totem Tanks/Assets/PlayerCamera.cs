﻿using UnityEngine;
using System.Collections;
using UnityEngine.Networking;

public class PlayerCamera : NetworkBehaviour
{
    public static Camera CurrentCam = null;
    public Camera TotemCam;
    public Vector3 BaseOffSet;
    public Vector3 VariableOffSet;

    // Use this for initialization
    public override void OnStartLocalPlayer(){
	    if (isLocalPlayer)
        {
            Camera.main.enabled = false;
            TotemCam.enabled = true;
            CurrentCam = TotemCam;
        }
	}
	
	// Update is called once per frame
	void Update () {
        if (isLocalPlayer)
        {
            //TotemCam.enabled = true;

            //if (this.GetComponent<PlayerTower>().TotemComponents.Count >= 2)
            {
                TotemCam.transform.localPosition = Vector3.Lerp(TotemCam.transform.localPosition,
                                                                BaseOffSet + 
                                                                VariableOffSet * this.GetComponent<PlayerTower>().TotemComponents.Count,
                                                                Time.deltaTime);
            }
        }
	}
}
