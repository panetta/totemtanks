﻿using UnityEngine;
using System.Collections;
using UnityEngine.Networking;

public class UserDetails : NetworkBehaviour {

    //public int HighScore;
    public string PlayerName;
    public int CurrentScore;

    public GameObject HighScoreSystem;

    public Scores scorekeeper;

    // Use this for initialization
    public override void OnStartLocalPlayer()
    {
        if (isLocalPlayer)
        {
            InvokeRepeating("CmdHSCalc", 1f, 1f);
            PlayerName = StaticManager.Instance.PlayerName;
            HighScoreSystem = GameObject.Find("HighScore");

            
            //scorekeeper.SetupClient();
            //PlayerNumber = int.Parse(StaticManager.Instance.PlayerName);
        }
    }
	
	// Update is called once per frame
	void Update () {

        CurrentScore = this.GetComponent<PlayerTower>().TotemComponents.Count;

        //scorekeeper.SendScore(CurrentScore, PlayerName);

    }

    [Command]
    void CmdHSCalc()
    {

        HighScoreSystem.GetComponent<HighScoreAccumulator>().HighScoreAmount = HighScoreSystem.GetComponent<HighScoreAccumulator>().HighScoreAmount + CurrentScore;

       

       HighScoreSystem.GetComponent<HighScoreAccumulator>().SendHS(HighScoreSystem.GetComponent<HighScoreAccumulator>().HighScoreAmount, PlayerName);

        //HighScoreAmount = HighScoreAmount + playersTower.topComponent;
        //CmdSendHS(HighScoreAmount, PlayerName);

    }


}
