﻿using UnityEngine;
using System.Collections;
using UnityEngine.Networking;
using UnityEngine.UI;

public class TowerDamageReceiver : NetworkBehaviour
{
    public float Health = 40f;
    public PlayerTower Owner;
    public bool IsBroken = false;
    private Collider _collider;
    private Vector3 _explosionDirection;
    private const float Gravity = 5.81f;

    public const float BrokenHeadRotationSpeed = 300f;

    public Image HealthContainer;
    public Image HealthImage;
    private float _initialHealth;

    public void Start()
    {
        _collider = GetComponentInChildren<Collider>();
        _initialHealth = Health;
    }

    public void ReceiveDamage(float angleAmount, Vector3 direction)
    {
        if (Owner != null)
        {
            Health -= angleAmount;
            if (Health <= 0f)
            {
                Owner.BreakTower(this.gameObject);
            }

            RpcUpdateHealthBar(Health, _initialHealth);
            //Disable bending towers for now
            //Owner.RpcReceiveDamage(angleAmount, direction);
        }
    }

    [ClientRpc]
    public void RpcUpdateHealthBar(float health, float initialHealth)
    {
        if (health < initialHealth - 1f)
        {
            HealthContainer.gameObject.SetActive(true);
            HealthImage.fillAmount = health / initialHealth;
        }
        else
        {
            HealthContainer.gameObject.SetActive(false);
        }
    }

    public void MakeBroken()
    {
        Owner = null;
        transform.parent = null;
        IsBroken = true;
        _collider.enabled = false;
        _explosionDirection = new Vector3(Random.Range(-Gravity, Gravity), 0f, Random.Range(-Gravity, Gravity));
    }

    public void Update()
    {
        if (IsBroken)
        {
            transform.position -= Vector3.up*Time.deltaTime*Gravity;
            transform.position += _explosionDirection*Time.deltaTime;
            transform.RotateAround(_collider.bounds.center, transform.right, BrokenHeadRotationSpeed);
            transform.RotateAround(_collider.bounds.center, transform.forward, BrokenHeadRotationSpeed);
            if (transform.position.y < -50f)
            {
                Destroy(gameObject);
            }
        }

        //if (Owner != null)
        //{
        //    if (Mathf.Abs(Quaternion.Angle(Owner.DamageController.transform.rotation, transform.rotation)) >
        //        Owner.DamageController.RotationBreakAngle)
        //    {
        //        Debug.Log("Broken");
        //        Owner.BreakTower(this.gameObject);
        //    }
        //}
    }
}
