﻿using UnityEngine;
using System.Collections;


public class StaticManager : MonoBehaviour {

    public string PlayerName;
    public AudioSource Music;
    public AudioSource TankRoll;
    public AudioSource Win;

    public AudioSource[] LargeCannonFire;
    int nextLargeCannonFire = 0;

    public AudioSource[] MediumCannonFire;
    int nextMediumCannonFire = 0;

    public AudioSource[] LaserCannonFire;
    int nextLaserCannonFire = 0;

    public AudioSource[] Explosion;
    int nextExplosion = 0;

    public AudioSource[] TotemPickUp;
    int nextTotemPickUp = 0;

    // Setup so that it's always available 
    private static StaticManager _instance = null;

    public static StaticManager Instance
    {
        get { return _instance; }
    }

    void Awake()
    {
        // Makes sure that there is only one of this object
        if (_instance != null && _instance != this)
        {
            // If it's not the main instance, then destroy it
            Destroy(gameObject);
            return;
        }
        else
        {
            _instance = this;
        }

        // Make sure this object isn't destroyed when you change scenes
        DontDestroyOnLoad(this.gameObject);
        gameObject.name = "$StaticManager";
    }




    // Use this for initialization
    void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {

    }

    public void PlayLargeCannonFire()
    {
        LargeCannonFire[nextLargeCannonFire].Play();
        nextLargeCannonFire++;
        if (nextLargeCannonFire >= LargeCannonFire.Length)
        {
            nextLargeCannonFire = 0;
        }
    }

    public void PlayMediumCannonFire()
    {
        MediumCannonFire[nextMediumCannonFire].Play();
        nextMediumCannonFire++;
        if (nextMediumCannonFire >= MediumCannonFire.Length)
        {
            nextMediumCannonFire = 0;
        }
    }

    public void PlayLaserCannonFire()
    {
        LaserCannonFire[nextLaserCannonFire].Play();
        nextLaserCannonFire++;
        if (nextLaserCannonFire >= LaserCannonFire.Length)
        {
            nextLaserCannonFire = 0;
        }
    }


    public void PlayExplosion()
    {
        Explosion[nextExplosion].Play();
        nextExplosion++;
        if (nextExplosion >= Explosion.Length)
        {
            nextExplosion = 0;
        }
    }

    public void PlayTotemPickUp()
    {
        TotemPickUp[nextTotemPickUp].Play();
        nextTotemPickUp++;
        if (nextTotemPickUp >= TotemPickUp.Length)
        {
            nextTotemPickUp = 0;
        }
    }

}
